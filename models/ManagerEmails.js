/*************************************************************************
POSTS TABLE
*************************************************************************/

module.exports = function(sequelize, Sequelize) {
    var ManagerEmails = sequelize.define('managerEmails', {
        BusinessId: {
            type: Sequelize.INTEGER,
            field: 'business_id'
        },
        DepartmentName: {
            type: Sequelize.STRING,
            field: 'department_name'
        },
        Email: {
            type: Sequelize.STRING,
            field: 'email'
        }        
    }, {
        freezeTableName: true
    });
    return ManagerEmails;
}