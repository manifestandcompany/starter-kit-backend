/*************************************************************************
BLOG POSTS CATEGORIES TABLE
*************************************************************************/

module.exports = function(sequelize, Sequelize) {
    
    const Postcategories = sequelize.define('postcategories', {
        postId: {
            type: Sequelize.INTEGER,
            field: 'postId'
        },
        categoryId: {
            type: Sequelize.INTEGER,
            field: 'categoryId',
        }
    }, {
        freezeTableName: true
    });

    Postcategories.associate = function(models) {
        models.postcategories.belongsTo(models.posts, {onDelete: 'CASCADE',targetKey: "id", foreignKey: 'postId'});
        models.postcategories.belongsTo(models.categories, {targetKey: "id", foreignKey: 'categoryId'});
    };

    return Postcategories;

}