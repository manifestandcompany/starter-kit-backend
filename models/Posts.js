/*************************************************************************
POSTS TABLE
*************************************************************************/

module.exports = function(sequelize, Sequelize) {
    var Posts = sequelize.define('posts', {
        Title: {
            type: Sequelize.STRING,
            field: 'title'
        },
        Description: {
            type: Sequelize.TEXT,
            field: 'description'
        },
        Status: {
            type: Sequelize.ENUM('publish', 'draft', 'verify', 'reject'),
            field: 'status',
            defaultValue: 'draft'
        },
        BusinessId: {
            type: Sequelize.INTEGER,
            field: 'businessId'
        },
        UserID: {
            type: Sequelize.STRING,
            field: 'userId'
        },
        UserName: {
            type: Sequelize.STRING,
            field: 'UserName'
        },
        RoleLevel: {
            type: Sequelize.INTEGER,
            field: 'roleLevel'
        },
        RoleName: {
            type: Sequelize.STRING,
            field: 'roleName'
        },
        DepartmentName: {
            type: Sequelize.STRING,
            field: 'departmentName'
        },
        Email: {
            type: Sequelize.STRING,
            field: 'email'
        },
        PublishedAt: {
            type: Sequelize.DATE,
            field: 'PublishedAt',
            defaultValue: null
        }
    }, {
        freezeTableName: true
    });

    Posts.associate = function(models) {
        models.posts.hasMany(models.comments, {onDelete: 'cascade',targetKey: "id", foreignKey: 'postId'});
        models.posts.hasMany(models.postcategories, {onDelete: 'cascade',targetKey: "id", foreignKey: 'postId'});
        models.posts.hasMany(models.media, {onDelete: 'cascade',targetKey: "id", foreignKey: 'postId'});
    };

    return Posts;

}