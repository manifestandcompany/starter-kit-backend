/*************************************************************************
BLOG POSTS CATEGORIES TABLE
*************************************************************************/

module.exports = function(sequelize, Sequelize) {
    
    const Categories = sequelize.define('categories', {
        Name: {
            type: Sequelize.STRING,
            field: 'name'
        },
        Status: {
            type: Sequelize.ENUM('active', 'deactivated'),
            field: 'status',
            defaultValue: 'active'
        }
    }, {
        freezeTableName: true
    });

    Categories.associate = function(models) {
        models.categories.hasMany(models.postcategories, {targetKey: "id", foreignKey: 'categoryId'});
    };

    return Categories;

}