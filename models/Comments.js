/*************************************************************************
COMMENTS TABLE
*************************************************************************/

module.exports = function(sequelize, Sequelize) {
    
    const Comments = sequelize.define('comments', {
        PostId: {
            type: Sequelize.INTEGER,
            field: 'postId'
        },
        UserId: {
            type: Sequelize.STRING,
            field: 'userId'
        },
        Comment: {
            type: Sequelize.STRING,
            field: 'description'
        },
        UserName: {
            type: Sequelize.STRING,
            field: 'name'
        },
        Status: {
            type: Sequelize.ENUM('pending', 'verify', 'reject'),
            field: 'status',
            defaultValue: 'pending'
        },
        Email: {
            type: Sequelize.STRING,
            field: 'email'
        },
        BusinessId: {
            type: Sequelize.INTEGER,
            field: 'businessId'
        },
        DepartmentName: {
            type: Sequelize.STRING,
            field: 'departmentName'
        },
    }, {
        freezeTableName: true
    });

    Comments.associate = function(models) {
        models.comments.belongsTo(models.posts, {onDelete: 'CASCADE',targetKey: "id", foreignKey: 'postId'});
    };



    return Comments;

}