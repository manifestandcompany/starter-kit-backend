/*************************************************************************
POSTS TABLE
*************************************************************************/

module.exports = function(sequelize, Sequelize) {
    var Media = sequelize.define('media', {
        PostId: {
            type: Sequelize.INTEGER,
            field: 'postId'
        },
        URL: {
            type: Sequelize.STRING,
            field: 'url'
        },
        Type: {
            type:Sequelize.STRING,
            field:'type'
        }
    }, {
        freezeTableName: true
    });

    Media.associate = function(models) {
        models.media.belongsTo(models.posts, {onDelete: 'CASCADE',targetKey: "id", foreignKey: 'postId'});
    };
    return Media;
}