const express = require('express')
const router = express.Router()

var config = require('../config');

var authenticate = require('../controllers/Auth/authentication')
var permissions = require('../controllers/Permissions/permissions')
var posts = require('../controllers/Posts/post')
var comments = require('../controllers/Comments/comments')
var media = require('../controllers/Media/media')
var general = require('../controllers/General/general');

var users = require('../controllers/Users/users')

const validator = require("../controllers/Validator/validator")
const { validationResult } = require('express-validator');

var aws = require('aws-sdk')
var multer = require('multer')
var multerS3 = require('multer-s3')

aws.config.update({
    secretAccessKey: config.SECRETKEY,
    accessKeyId: config.ACCESSKEY,
    region: config.REGION
});
const s3 = new aws.S3();

var upload = multer({
    storage: multerS3({
      s3: s3,
      bucket: config.BUCKETNAME,
      metadata: function (req, file, cb) {
        cb(null, {fieldName: file.fieldname});
      },
      key: function (req, file, cb) {
        cb(null, Date.now().toString())
      }
    })
  })

/*************************************************************************
API CALL START
*************************************************************************/

/*************************************************************************
CREATE JWT TOKEN
*************************************************************************/
router.post('/',authenticate.createJWTToken)

/*************************************************************************
CRETE JWT TOKEN
*************************************************************************/
router.post('/auth', authenticate.createJWTToken)
/*************************************************************************

 
 
/*************************************************************************
CREATE POSTS API (CreateOnePost)
*************************************************************************/
router.post('/posts/create',validator.validate('/posts/create'), permissions.hasCreatePermission, posts.createPost)

/*************************************************************************
CREATE MULTIPLE POSTS API (CreateMultiplePosts)
*************************************************************************/
router.post('/posts/createMultiple',validator.validate('/posts/createMultiple'), permissions.hasCreatePermission, posts.createMultiplePosts)


/*************************************************************************
GET POSTS API (ListAllPosts)
*************************************************************************/
router.post('/posts/get',validator.validate('/posts/get'), permissions.hasReadPermission, posts.getPosts)

/*************************************************************************
GET ONE POST API (ListOnePost)
*************************************************************************/
router.post('/posts/getOne',validator.validate('/posts/getOne'), permissions.hasReadPermission, posts.getPostDetail)

/*************************************************************************
GET POST DETAIL API
*************************************************************************/
router.post('/posts/getDetail',validator.validate('/posts/getDetail'), permissions.hasReadPermission, posts.getPostDetail)

/*************************************************************************
GET CATEGORIES
*************************************************************************/
router.get('/categories', permissions.hasReadPermission, posts.getCategories)

/*************************************************************************
Create CATEGORIES
*************************************************************************/
router.post('/categories/create',validator.validate('/categories/create'), permissions.hasCreatePermission, posts.createCategories)

/*************************************************************************
GET POST DETAIL WITH COMMENTS API (ReadOnePost)
*************************************************************************/
router.post('/posts/getProductDetail', validator.validate('/posts/getProductDetail'), permissions.hasReadPermission, posts.getPostDetailWithComments)

/*************************************************************************
GET MULTIPLE POST DETAILS WITH COMMENTS API (ReadAllPosts)
*************************************************************************/
router.post('/posts/getMultipleProductsDetail',validator.validate('/posts/getMultipleProductsDetail'), permissions.hasReadPermission, posts.getMultiplePostsDetailWithComments)

/*************************************************************************
UPDATE POSTS API (UpdateOnePost)
*************************************************************************/
router.post('/posts/update', validator.validate('/posts/update'), permissions.hasEditPermission, posts.updatePost)

/*************************************************************************
UPDATE MULTIPLE POSTS API (UpdateMultiplePosts)
*************************************************************************/
router.post('/posts/updateMultiple',validator.validate('/posts/updateMultiple'), permissions.hasEditPermission, posts.updateMultiplePosts)


/*************************************************************************
UPDATE POSTS STATUSES API
*************************************************************************/
router.post('/posts/updateMultipleStatuses',validator.validate('/posts/updateMultipleStatuses'), permissions.hasManagerRole, posts.udpatePostStatuses)


/*************************************************************************
UPDATE POST STATUS API
*************************************************************************/
router.post('/posts/updateStatus', validator.validate('/posts/updateStatus'), permissions.hasManagerRole, posts.updateStatus)


/*************************************************************************
DELETE POST API (DeleteOnePost)
*************************************************************************/
router.post('/posts/delete',validator.validate('/posts/delete'), permissions.hasDeletePermission, posts.deletePost)

/*************************************************************************
DELETE MULTIPLE POSTS API (DeleteMultiplePosts)
*************************************************************************/
router.post('/posts/deleteMultiplePosts',validator.validate('/posts/deleteMultiplePosts'), permissions.hasDeletePermission, posts.deleteMultiplePosts)

/*************************************************************************
DELETE ALL POSTS API (DeleteAllPosts)
*************************************************************************/
router.post('/posts/deleteAllPosts', permissions.hasDeletePermission, posts.deleteAllPosts)


/*************************************************************************
UPLOAD FILE TO S3
*************************************************************************/
router.post('/posts/uploadFileToS3', upload.array('uploadedFiles',1), posts.uploadFile)


/*************************************************************************
GET S3 FILE DOWNLOAD URL
*************************************************************************/
router.get('/getDownloadUrl',general.getDownloadUrl)


/*************************************************************************
FETCH S3 SIGNATURE TO UPLOAD FILES ON S3
*************************************************************************/
router.post('/fetchSignature', general.getSignature) 

/*************************************************************************
CREATE COMMENT (CreateOneComment)
*************************************************************************/
router.post('/comments/create',validator.validate('/comments/create'), permissions.hasCreatePermission, comments.createComment) 

/*************************************************************************
CREATE MULTIPLE COMMENTS (CreateMultipleComments)
*************************************************************************/
router.post('/comments/createMultiple',validator.validate('/comments/createMultiple'), permissions.hasCreatePermission, comments.createMultipleComments) 

/*************************************************************************
UPDATE COMMENT (UpdateOneComment)
*************************************************************************/
router.post('/comments/update',validator.validate('/comments/update') , permissions.hasEditPermission, comments.updateComment) 

/*************************************************************************
UPDATE MULTIPLE COMMENT (UpdateMultipleComments)
*************************************************************************/
router.post('/comments/updateMultiple',validator.validate('/comments/updateMultiple'), permissions.hasEditPermission, comments.updateMultipleComments) 

/*************************************************************************
GET COMMENTS LIST (ListAllComments,ReadAllComments)
*************************************************************************/
router.post('/comments/get',validator.validate('/comments/get'), permissions.hasReadPermission, comments.getComments) 

/*************************************************************************
GET ONE COMMENT API (ListOneComment,ReadOneComment)
*************************************************************************/
router.post('/comments/getOne',validator.validate('/comments/getOne'), permissions.hasReadPermission, comments.getCommentDetail)

/*************************************************************************
DELTE COMMENT (DeleteOneComment)
*************************************************************************/
router.post('/comments/delete',validator.validate('/comments/delete'), permissions.hasDeletePermission, comments.deleteComment)

/*************************************************************************
DELETE MULTIPLE COMMENTS API (DeleteMultipleComments)
*************************************************************************/
router.post('/comments/deleteMultipleComments',validator.validate('/comments/deleteMultipleComments'), permissions.hasDeletePermission, comments.deleteMultipleComments)

/*************************************************************************
DELETE ALL COMMENTS API (DeleteAllComments)
*************************************************************************/
router.post('/comments/deleteAllComments', permissions.hasDeletePermission, comments.deleteAllComments)

/*************************************************************************
UDPATE COMMENT STATUS
*************************************************************************/
router.post('/comments/udpateCommentStatus',validator.validate('/comments/udpateCommentStatus'), permissions.hasManagerRole, comments.updateCommentStatus)

/*************************************************************************
GET MEDIA API (ListAllMedia)
*************************************************************************/
router.post('/media/get', permissions.hasReadPermission, media.getMedia)

/*************************************************************************
DELTE MEDIA (DeleteOneMedia)
*************************************************************************/
router.post('/media/delete',validator.validate('/media/delete'), permissions.hasDeletePermission, media.deleteMedia)

/*************************************************************************
DELETE MULTIPLE MEDIAS API (DeleteMultipleMedias)
*************************************************************************/
router.post('/media/deleteMultipleMedias',validator.validate('/media/deleteMultipleMedias'), permissions.hasDeletePermission, media.deleteMultipleMedias)

/*************************************************************************
GET SETTINGS API (listAllSettings)
*************************************************************************/
router.post('/settings/get', permissions.hasManagerRole, general.getSettings)

/*************************************************************************
UPDATE SETTINGS API (updateAllSettings)
*************************************************************************/
router.post('/settings/updateAll', permissions.hasManagerRole, general.updateAllSettings)


/*************************************************************************
CREATE LOGIN ROUTE TO TEST POSTMAN
*************************************************************************/
router.post('/login',authenticate.getUserToken)

/*************************************************************************
GET ALL USERS FOR A BUSINESS FROM THE USERS MODULE
*************************************************************************/
router.post('/users',users.getUsersList)

module.exports = router;