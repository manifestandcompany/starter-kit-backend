// IMPORT DATABASE & CONFIG FILE
var DB = require('../DB/db')
const config =  require('../../config')
const fetch = require("node-fetch")
const general = require('../General/general')

/**
 * GET USER LISTS FROM THE USER MODULE
 * @param {*} accountId 
 */
const getUsersListFromUserModule = async (res,accountId) =>{

    try{
    
        let result = await DB.token.findOne({
            where:{
                UID: accountId,
            }
        })
        
        let map = {};
    
        let api_name = await general.getSetting(res,'API_NAME_GET_USERS_LIST')

        map['app_key'] = result.get('KeysHash');
        map['api'] = api_name;
        
        let jsonmap = JSON.stringify(map);
        
        let response = await fetch(config.USER_MODULE_API_URL,{
            method:'POST',
            async:false,
            body:jsonmap,
            headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json'}
        });
            
        const ret_data = await response.json();
        
        console.log('This is the returned data: ' + ret_data.data);
        
        return res.status(200).send({users: ret_data.data}); 
    
    }catch(error){
            console.log(error)
        }
}

const  getUsersList = async (req, res, next) => {
    var users = await getUsersListFromUserModule(res,res.locals.accountId)
    return {users: users} //gives all users but you need to filter through response to get the data you want
}


module.exports = {
   getUsersListFromUserModule,getUsersList
}