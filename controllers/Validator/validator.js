const { body } = require('express-validator')

exports.validate = (method) => {
  switch (method) {
    case '/categories/create': {
      return [ 
          body('categories').custom(value => { return Array.isArray(value) }),
        ]   
    }
    case '/media/delete': {
      return [ 
          body('mediaId').custom(value => { return Number(value) }),
        ]   
    }
    case '/posts/getOne': {
      return [ 
          body('postId').not().isEmpty(),
        ]   
    }
    case '/comments/deleteMultipleComments': {
      return [ 
          body('ids').custom(value => { return Array.isArray(value) }),
      ]   
    }
    case '/comments/delete': {
      return [ 
          body('commentId').custom(value => { return Number(value) }),
      ]   
    }
    case '/comments/getOne': {
      return [ 
          body('commentId').custom(value => { return Number(value) }),
      ]   
    }
    case '/comments/create': {
      return [ 
          body('post.status').exists().isIn(['pending']),
          body('post.comment').exists().not().isEmpty(),
          body('post.postId').custom(value => { return Number(value) }),
      ]   
    }
    case '/comments/update': {
      return [ 
          body('comment').exists().not().isEmpty(),
          body('id').custom(value => { return Number(value) }),
      ]   
    }
    case '/posts/getProductDetail': {
      return [ 
          body('postId').custom(value => { return Number(value) }),
        ]   
    }
    case '/posts/getDetail': {
      return [ 
          body('postId').custom(value => { return Number(value) }),
        ]   
    }
    case '/posts/get': {
      return [ 
          body('status').optional().isString(),
          body('category').optional().isString(),
        ]   
    }
    case '/posts/updateStatus': {
      return [ 
          body('postId').custom(value => { return Number(value) }),
          body('status').not().isEmpty(),
        ]   
    }
    case '/posts/delete': {
      return [ 
          body('postId').custom(value => { return Number(value) }),
        ]   
    }
    case '/posts/create': {
      return [ 
          body('post.title').not().isEmpty().isString(),
          body('post.description').exists().isString(),
          body('post.category').custom(value => { return Array.isArray(value) }),
          body('post.status').exists().isIn(['draft', 'publish'])
        ]   
    }
    case '/posts/update': {
      return [ 
          body('post.id').custom(value => { return Number(value) }),
          body('post.title').not().isEmpty().isString(),
          body('post.description').exists().isString(),
          body('post.category').custom(value => { return Array.isArray(value) }),
          body('post.status').exists().isIn(['draft', 'publish','reject','verify'])
        ]   
    }
    case '/posts/createMultiple': {
      return [
         body('posts').custom(value => { return Array.isArray(value) }),
        ]   
    }
    case '/posts/getMultipleProductsDetail': {
      return [
         body('postIds').custom(value => { return Array.isArray(value) }),
        ]   
    }
    case '/posts/updateMultiple': {
      return [
         body('posts').custom(value => { return Array.isArray(value) }),
        ]   
    }
    case '/posts/deleteMultiplePosts': {
      return [
         body('ids').custom(value => { return Array.isArray(value) }),
        ]   
    }
    case '/posts/updateMultipleStatuses': {
      return [
         body('ids').custom(value => { return Array.isArray(value) }),
        ]   
    }
    case '/media/deleteMultipleMedias': {
      return [
         body('ids').custom(value => { return Array.isArray(value) }),
        ]   
    }
    case '/comments/createMultiple': {
      return [
         body('comments').custom(value => { return Array.isArray(value) }),
        ]   
    }
    case '/comments/updateMultiple': {
      return [
         body('comments').custom(value => { return Array.isArray(value) }),
        ]   
    }
    case '/comments/get': {
      return [ 
          body('status').optional().isString(),
      ]   
    }
    case '/comments/udpateCommentStatus': {
      return [
          body('commentId').custom(value => { return Number(value) }),
          body('status').not().isEmpty(),
        ]   
    }
    case '/test': {
     return [
        body('posts').custom(value => { return Array.isArray(value) }),
       ]   
    }
  }
}