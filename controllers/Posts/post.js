// IMPORT DATABASE FILE
var DB = require('../DB/db')
const { Op } = require('sequelize');
const general = require('../General/general')
const { validationResult } = require('express-validator');

/**
 * CREATE MULtiPLE POSTS
 * @param {posts array containing post objects} req 
 * @param {*} res 
 * @param {*} next 
 */

const createMultiplePosts= async (req, res, next) => {

        try{

            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            // CREATE POSTS IN DATABASE

            let posts = req.body.posts

            for(let i=0;i<posts.length;i++){

                let post_data = posts[i]

                let insert_data = {
                    Title: post_data.title,
                    Description: post_data.description,
                    Status: post_data.status,
                    BusinessId: res.locals.businessId,
                    Email: res.locals.userEmail,
                    UserID: res.locals.accountId,
                    UserName:res.locals.userName,
                    RoleLevel: res.locals.roleLevel,
                    RoleName: res.locals.roleName,
                    DepartmentName: res.locals.departmentName
                }
    
                //we need to set published, if status is publish
                if(post_data.status === 'publish'){
                    insert_data.PublishedAt = new Date(Date.now()).toISOString();
                }
    
                var result = await DB.posts.create(insert_data);
                
                result = result.dataValues

                //insert post categories
                let cateogry_ids = post_data.category

                for(let i=0;i<cateogry_ids.length;i++){
                    
                    let insert_data = {
                        postId:result.id,
                        categoryId:Number(cateogry_ids[i])
                    }

                    await DB.postcategories.create(insert_data);

                }

                // SEND PUBLISHED POST CREATION EMAIL TO MANAGER
                if(post_data.status === 'publish'){
                    let sendEmailData = {subject:"New Post Created",client_base_url:req.headers.client_base_url,businessId: res.locals.businessId, departmentName: res.locals.departmentName}
                    await general.prepareEmail(res,'POSTCREATIONTEMPLATE',sendEmailData)
                }

            }

            // RETURN RESPONSE
            return res.status(200).send({post:result})
        }
        catch(error){
            console.log(error)
            // RETURN RESPONSE
            return res.status(400).send(JSON.stringify({message: error.message}))
        }
}

/**
 * get blog categories
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */

const getCategories = async (req, res, next) => {
    try{
        // get categories

        let categories = await DB.categories.findAll({
            where:{
                Status:'active'
            }
        });

        // RETURN RESPONSE
        return res.status(200).send({categories:categories})
    }
    catch(error){
        console.log(error)
        // RETURN RESPONSE
        return res.status(400).send(JSON.stringify({message: error.message}))
    }
}

/**
 * create one or more blog categories
 * @param {categories array} req 
 * @param {*} res 
 * @param {*} next 
 */

const createCategories = async (req,res,next) => {
    try{

        let categories = req.body.categories

        for(let i=0;i<categories.length;i++){

            let cat = categories[i]

            //lets check that we don't have this category already

            let check_cat = await DB.categories.findOne({
                where:{
                    Name: {
                        [DB.Sequelize.Op.iLike]: '%'+cat+'%'
                    }
                }
            });

            if(check_cat){
                //this category already exists, so looks like it is deactivated, so lets activate it
                let cat_id = check_cat.dataValues.id

                await DB.categories.update({Status:'active'},
                {
                    where: {
                        id: cat_id
                    }
                })
                
            }
            else{
                //insert the category
                await DB.categories.create({Name:cat});

            }

        }

        return res.status(200).send(JSON.stringify({status:1,"message":"Categories created successfully"}))

    }
    catch(error){
        return res.status(400).send(JSON.stringify({message: error.message}))
    }
}

/**
 * CREATE SINGLE POST
 * @param {post object} req 
 * @param {*} res 
 * @param {*} next 
 */

const createPost= async (req, res, next) => {

        try{

            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            // CREATE POST IN DATABASE

            let insert_data = {
                Title: req.body.post.title,
                Description: req.body.post.description,
                Status: req.body.post.status,
                BusinessId: res.locals.businessId,
                Email: res.locals.userEmail,
                UserID: res.locals.accountId,
                UserName:res.locals.userName,
                RoleLevel: res.locals.roleLevel,
                RoleName: res.locals.roleName,
                DepartmentName: res.locals.departmentName
            }

            //we need to set published, if status is publish
            if(req.body.post.status === 'publish'){
                insert_data.PublishedAt = new Date(Date.now()).toISOString();
            }

            let result = await DB.posts.create(insert_data);
            result = result.dataValues

            //insert post media, if there is any

            let media = req.body.post.media
            for(let media_i=0;media_i<media.length;media_i++){

                await DB.media.create({PostId:result.id,URL:media[media_i].URL,Type:media[media_i].Type})

            }

            //insert post categories
            let cateogry_ids = req.body.post.category

            for(let i=0;i<cateogry_ids.length;i++){
                
                let insert_data = {
                    postId:result.id,
                    categoryId:Number(cateogry_ids[i])
                }

                await DB.postcategories.create(insert_data);

            }

            // SEND PUBLISHED POST CREATION EMAIL TO MANAGER
            if(req.body.post.status === 'publish'){
                let sendEmailData = {subject:"New Post Created",client_base_url:req.headers.client_base_url,businessId: res.locals.businessId, departmentName: res.locals.departmentName}
                await general.prepareEmail(res,'POSTCREATIONTEMPLATE',sendEmailData)
            }

            let categories = await DB.categories.findAll({
                where:{
                    Status:'active'
                }
            });

            result.categories = categories
    
            let post_data = await DB.posts.findOne({
                where:{
                    id:result.id
                },
                include:[
                    {
                        model:DB.postcategories
                    }
                ]
            })

            result.postcategories = post_data.dataValues.postcategories

            // RETURN RESPONSE
            return res.status(200).send({post:result})
        }
        catch(error){
            console.log(error)
            // RETURN RESPONSE
            return res.status(400).send(JSON.stringify({message: error.message}))
        }
}

/**
 * GET POSTS FROM DATABASE, CALLED FROM POSTS LISTING PAGE
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */

const getPosts = async (req, res, next) => {

    try{
        // GET ALL POST FROM DATABASE

        const errors = validationResult(req);
        
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        let status = "all"
        if(req.body.hasOwnProperty('status') && req.body.status!=""){
            status = req.body.status
        }

        let category = "all"
        if(req.body.hasOwnProperty('category') && req.body.category!=""){
            category = req.body.category
        }

        let where = {
            BusinessId: res.locals.businessId,
            RoleLevel:{
                [Op.gte]: res.locals.roleLevel
            }
        }

        if(status!=="" && status!=="all"){
            where.Status = status
        }

        let include = []

        let include_postcategories = {
            model:DB.postcategories
        }

        if(category!=="" && category!=="all"){
            include_postcategories.where = {
                categoryId:parseInt(category)
            }
        }

        include.push(include_postcategories)

        include.push({
            model:DB.media
        })

        let post = await DB.posts.findAll({
            where:where,
            include:include
        })

        let categories = await DB.categories.findAll({
            where:{
                Status:'active'
            }
        });

        let posts = []
        // IF NO POSTS IN DATABASE
        if(post.length === 0){
            return res.status(200).send(JSON.stringify({categories,entities:[]}))
        }
        else{
            // LOOP OVER ALL POST
            for(let i=0;i<post.length;i++) {

                let postcategories = post[i].get('postcategories')

                let categories = []

                //if this post has categories
                if(postcategories.length){
                    for(let j=0;j<postcategories.length;j++){
                        let category_id = postcategories[j].dataValues.categoryId

                        //get this category name

                        let category_data = await DB.categories.findOne({
                            where:{
                                id:category_id
                            }
                        })

                        if(category_data){
                            categories.push(category_data.get('Name'))
                        }

                    }
                }

                let push_data = {
                    id: post[i].get('id'),
                    media:post[i].get('media'),
                    description: post[i].get('Description'),
                    title: post[i].get('Title'),
                    categories:categories,
                    PublishedAt: post[i].get('PublishedAt'),
                    UserName: post[i].get('UserName'),
                    status: post[i].get('Status')
                }

                posts.push(push_data)
            }

            // RETURN SUCCESS RESPONSE
            return res.status(200).send(JSON.stringify({categories,entities:posts, totalCount: post.length}))
        }
    }
    catch(error){
        // RETURN ERRIR RESPONSE
        console.log(error.message)
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

/**
 * GET POST DETAIL FROM DATABASE
 * @param {postId} req 
 * @param {*} res 
 */

const getPostDetail = async(req,res) => {
    try{

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        // GET POST DATA FROM POST ID
        let post = await DB.posts.findOne({
            where:{
                id: req.body.postId
            },
            include:[
                {
                    model:DB.postcategories
                },
                {
                    model:DB.media
                }
            ]
        })
        let postDetail = ''
        // IF POST NOT FOUND
        if(post === null){
            return res.status(200).send()
        }
        else{
            // GET POST DETAIL

            //get all categories to show in the dropdown

            let categories = await DB.categories.findAll({
                where:{
                    Status:'active'
                }
            })
            
            postDetail = {media:post.get('media'),categories:categories,id: post.get('id'),postcategories:post.get('postcategories'),description: post.get('Description'), title: post.get('Title'), status: post.get('Status')}
            // RETURN SUCCESS RESPONSE
            return res.status(200).send(postDetail)
        }
    }
    catch(error){
        // RETURN ERROR RESPONSE
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

/**
 * GET MULTIPLE POSTS DETAIL WITH COMMENTS FROM DATABASE
 * @param {array containing postIds} req 
 * @param {*} res 
 */

const getMultiplePostsDetailWithComments = async(req,res) => {
    try{

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        let postIds = req.body.postIds

        let posts = []

        for(let i=0;i<postIds.length;i++){

            let postId = postIds[i]

            // GET POST DATA FROM POST ID
            let post = await DB.posts.findOne({
                where:{
                    id: postId
                },
                include:[
                    {
                        model:DB.postcategories
                    },
                    {
                        model:DB.media
                    }
                ]
            })
            // GET POST DATA FROM POST ID
            let commentsList = await DB.comments.findAll({
                where:{
                    PostId: postId,
                    Status: 'verify'
                }
            })
            let postDetail = {}
            let comments = []
            // IF POST NOT FOUND
            if(post === null){
                return res.status(200).send()
            }
            else{
                // GET POST DETAIL
                postDetail = {id: post.get('id'), description: post.get('Description'), title: post.get('Title'), status: post.get('Status')}
                // LOOP OVER ALL POST
                for(let i=0;i<commentsList.length;i++) {
                    comments.push({'name':  commentsList[i].get('UserName'),'detail':commentsList[i].get('Comment')})
                }

                //put comments in the post detail
                postDetail.comments = comments

            }

            posts.push(postDetail)

        }

        // RETURN SUCCESS RESPONSE
        return res.status(200).send({posts: posts})

    }
    catch(error){
        // RETURN ERROR RESPONSE
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

/**
 * GET POST DETAIL WITH COMMENTS FROM DATABASE
 * @param {postId} req 
 * @param {*} res 
 */

const getPostDetailWithComments = async(req,res) => {
    try{

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        // GET POST DATA FROM POST ID
        let post = await DB.posts.findOne({
            where:{
                id: req.body.postId
            },
            include:[
                {
                    model:DB.postcategories
                },
                {
                    model:DB.media
                }
            ]
        })

        // GET POST DATA FROM POST ID
        let commentsList = await DB.comments.findAll({
            where:{
                PostId: req.body.postId,
                Status: 'verify'
            }
        })
        let postDetail = ''
        let comments = []
        // IF POST NOT FOUND
        if(post === null){
            return res.status(200).send()
        }
        else{

            //lets get post categories
            let categories = []
            let postcategories = post.get('postcategories')

            if(postcategories.length){
                for(let j=0;j<postcategories.length;j++){
                    let category_id = postcategories[j].dataValues.categoryId

                    //get this category name

                    let category_data = await DB.categories.findOne({
                        where:{
                            id:category_id
                        }
                    })

                    if(category_data){
                        categories.push(category_data.get('Name'))
                    }

                }
            }

            // GET POST DETAIL
            postDetail = {id: post.get('id'), media:post.get('media'),description: post.get('Description'),categories:categories, title: post.get('Title'), status: post.get('Status')}
            // LOOP OVER ALL POST
            for(let i=0;i<commentsList.length;i++) {
                comments.push({'name':  commentsList[i].get('UserName'),'detail':commentsList[i].get('Comment')})
            }
            // RETURN SUCCESS RESPONSE
            return res.status(200).send({post: postDetail, comments})
        }
    }
    catch(error){
        // RETURN ERROR RESPONSE
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

/**
 * UPDATE MULTIPLE POSTS IN DATABASE
 * @param {array of post objects to update} req 
 * @param {*} res 
 */

const updateMultiplePosts = async (req,res) => { 
    try{

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        let posts = req.body.posts

        for(let i=0;i<posts.length;i++){

            let post_data = posts[i]

            // UPDATE POST IN DATABASE

            let update_data = {
                Title: post_data.title,
                Description: post_data.description,
                Status: post_data.status,
                BusinessId: res.locals.businessId,
                UserID: res.locals.accountId,
                RoleLevel: res.locals.roleLevel,
                RoleName: res.locals.roleName,
                DepartmentName: res.locals.departmentName
            }

            //we need to set publishedAt, if status is publish
            //but we need to check whether this post is already published or it is being published now
            //because we don't want the issue that the publishedAt is populated everytime the post is updated
            if(post_data.status === 'publish'){

                let post_data_temp = await DB.posts.findOne({
                    where:{
                        id:post_data.id
                    }
                })

                if(post_data_temp.dataValues.Status!=="publish"){

                    //so now we are sure that the status of this post was not publish before, so this post has publishedAt null
                    //we need to populate it

                    update_data.PublishedAt = new Date(Date.now()).toISOString();
                }
                
            }
            else{
                update_data.PublishedAt = null
            }

            await DB.posts.update(update_data,
            {
                where: {
                    id: post_data.id
                }
            })

            //insert post media, if there is any

            //first delete them
            await DB.media.destroy({
                where:{
                    postId:post_data.id
                }
            })

            let media = post_data.media
            for(let media_i=0;media_i<media.length;media_i++){

                await DB.media.create({PostId:post_data.id,URL:media[media_i].URL,Type:media[media_i].Type})

            }

            //now let's update categories of the post

            //first delete them
            await DB.postcategories.destroy({
                where:{
                    postId:post_data.id
                }
            })

            //now insert them one by one

            let cateogry_ids = post_data.category

            for(let i=0;i<cateogry_ids.length;i++){
                
                let insert_data = {
                    postId:post_data.id,
                    categoryId:Number(cateogry_ids[i])
                }

                await DB.postcategories.create(insert_data);

            }

            // // SEND PUBLISHED POST CREATION EMAIL
            if(post_data.status === 'publish'){
                // SEND EMAIL TO MANAGER
                let sendEmailData = {subject:"Post Updated",client_base_url:req.headers.client_base_url,businessId: res.locals.businessId, departmentName: res.locals.departmentName}
                await general.prepareEmail(res,'POSTCREATIONTEMPLATE',sendEmailData)

                // SEND EMAIL TO CREATOR
                sendEmailData = {subject:"Post Updated",client_base_url:req.headers.client_base_url,postId: post_data.id}
                await general.prepareEmail(res,'POSTUPDATETEMPLATE',sendEmailData)
            }

        }

        // RETURN SUCCESS RESPONSE
        return res.status(200).send()
    }
    catch(error){
        
        // RETURN ERROR RESPONSE
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

/**
 * UPDATE SINGLE POST IN DATABASE
 * @param {post object} req 
 * @param {*} res 
 */

const updatePost = async (req,res) => {

    try{
        // UPDATE POST IN DATABASE

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        let update_data = {
            Title: req.body.post.title,
            Description: req.body.post.description,
            Status: req.body.post.status,
            BusinessId: res.locals.businessId,
            UserID: res.locals.accountId,
            RoleLevel: res.locals.roleLevel,
            RoleName: res.locals.roleName,
            DepartmentName: res.locals.departmentName
        }

        //we need to set publishedAt, if status is publish
        //but we need to check whether this post is already published or it is being published now
        //because we don't want the issue that the publishedAt is populated everytime the post is updated
        if(req.body.post.status === 'publish'){

            let post_data_temp = await DB.posts.findOne({
                where:{
                    id:req.body.post.id
                }
            })

            if(post_data_temp.dataValues.Status!=="publish"){

                //so now we are sure that the status of this post was not publish before, so this post has publishedAt null
                //we need to populate it

                update_data.PublishedAt = new Date(Date.now()).toISOString();
            }
            
        }
        else{
            update_data.PublishedAt = null
        }


        await DB.posts.update(update_data,
        {
            where: {
                id: req.body.post.id
        }})

        //insert post media, if there is any

        //first delete them
        await DB.media.destroy({
            where:{
                postId:req.body.post.id
            }
        })

        let media = req.body.post.media
        for(let media_i=0;media_i<media.length;media_i++){

            await DB.media.create({PostId:req.body.post.id,URL:media[media_i].URL,Type:media[media_i].Type})

        }

        //now let's update categories of the post

        //first delete them
        await DB.postcategories.destroy({
            where:{
                postId:req.body.post.id
            }
        })

        //now insert them one by one

        let cateogry_ids = req.body.post.category

        for(let i=0;i<cateogry_ids.length;i++){
            
            let insert_data = {
                postId:req.body.post.id,
                categoryId:Number(cateogry_ids[i])
            }

            await DB.postcategories.create(insert_data);

        }

        // // SEND PUBLISHED POST CREATION EMAIL
        if(req.body.post.status === 'publish'){
            // SEND EMAIL TO MANAGER
            let sendEmailData = {subject:"Post Updated",client_base_url:req.headers.client_base_url,businessId: res.locals.businessId, departmentName: res.locals.departmentName}
            await general.prepareEmail(res,'POSTCREATIONTEMPLATE',sendEmailData)

            // SEND EMAIL TO CREATOR
            sendEmailData = {subject:"Post Updated",client_base_url:req.headers.client_base_url,postId: req.body.post.id}
            await general.prepareEmail(res,'POSTUPDATETEMPLATE',sendEmailData)
        }

        let categories = await DB.categories.findAll({
            where:{
                Status:'active'
            }
        });

        let post_data = await DB.posts.findOne({
            where:{
                id:req.body.post.id
            },
            include:[
                {
                    model:DB.postcategories
                }
            ]
        })

        // RETURN SUCCESS RESPONSE
        return res.status(200).send({categories:categories,postcategories:post_data.dataValues.postcategories})
    }
    catch(error){
        console.log(error.message)
        // RETURN ERROR RESPONSE
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

/**
 * UPDATE POSTS STATUSES IN DATABASE
 * @param {postIds,status} req 
 * @param {*} res 
 */

const udpatePostStatuses = async (req,res) => { 
    try{
        
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        
        let postsIds = req.body.ids
        // LOOP OVER ALL POST TO UPDATE STATUS
        for(let i=0;i<postsIds.length;i++){
            // UPDATE POST IN DATABASE
            await DB.posts.update({
                Status: req.body.status,
            },
            {
                where: {
                    id: postsIds[i]
            }})
        }

        // SEND UPDATE STATUS EMAIL TO CREATOR
        let sendEmailData = {subject:"Post Status Changed",client_base_url:req.headers.client_base_url,postIds: postsIds, status:req.body.status}
        await general.prepareEmail(res,'POSTSTATUSUPDATETEMPLATE',sendEmailData)
        
        // RETURN SUCCESS RESPONSE
        return res.status(200).send()
    }
    catch(error){
        
        // RETURN ERROR RESPONSE
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

/**
 * UPDATE SINGLE POST STATUS IN DATABASE
 * @param {postId, status} req 
 * @param {*} res 
 */

const updateStatus = async (req,res) => { 
    try{

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        let {postId, status} = req.body
        // UPDATE POST IN DATABASE
        await DB.posts.update({
            Status: status,
        },
        {
            where: {
                id: postId
        }})

        // SEND UPDATE STATUS EMAIL TO CREATOR
        let sendEmailData = {subject:"Post Status Changed",client_base_url:req.headers.client_base_url,postIds: [postId], status:req.body.status}
        await general.prepareEmail(res,'POSTSTATUSUPDATETEMPLATE',sendEmailData)

        // RETURN SUCCESS RESPONSE
        return res.status(200).send()
    }
    catch(error){
        console.log(error.message)
        // RETURN ERROR RESPONSE
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

/**
 * PHYSICALLY DELETE MULTIPLE POSTS IN DATABASE
 * @param {ids} req 
 * @param {*} res 
 */

const deletePosts = async (req,res) => { 
    try{

        if(req.body.hasOwnProperty('ids') && req.body.ids.length){
            // UPDATE MULTIPLE POSTS IN DATABASE
            let {ids} = req.body

            for(let i=0;i<ids.length;i++){
                
                await DB.posts.destroy({
                    where: {
                        id: ids[i]
                }});
                
            }

            let categories = await DB.categories.findAll({
                where:{
                    Status:'active'
                }
            });

            // RETURN SUCCESS RESPONSE
            return res.status(200).send({categories})

        }
        else{
            return res.status(200).send()
        }
        
    }
    catch(error){
        console.log(error.message)
        // RETURN ERROR RESPONSE
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

/**
 * PHYSICALLY DELETE SINGLE POST FROM DATABASE
 * @param {postId} req 
 * @param {*} res 
 */

const deletePost = async (req,res) => { 
    try{

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        
        let {postId} = req.body
        //DELETE POST IN DATABASE
        await DB.posts.destroy({
            where: {
                id: postId
            },
            force: true
        });

        let categories = await DB.categories.findAll({
            where:{
                Status:'active'
            }
        });

        // RETURN SUCCESS RESPONSE
        return res.status(200).send({categories})
    }
    catch(error){
        console.log(error.message)
        // RETURN ERROR RESPONSE
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

/**
 * PHYSICALLY DELETE ALL POSTS FROM DATABASE
 * @param {*} req 
 * @param {*} res 
 */

const deleteAllPosts = async (req,res) => { 
    try{
        
        //Delete all posts in the database

        DB.posts.destroy({
            force: true,
            where: {}
        })

        // RETURN SUCCESS RESPONSE
        return res.status(200).send()
    }
    catch(error){
        console.log(error.message)
        // RETURN ERROR RESPONSE
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

/**
 * PHYSICALLY DELETE MULTIPLE POSTS FROM DATABASE
 * @param {ids} req 
 * @param {*} res 
 */

const deleteMultiplePosts = async (req,res) => { 
    try{

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        let ids = req.body.ids
        // LOOP OVER ALL POST TO UPDATE STATUS
        for(let i=0;i<ids.length;i++){
            // UPDATE POST IN DATABASE
            await DB.posts.destroy({
                where: {
                    id: ids[i]
            }})
        }
        // RETURN SUCCESS RESPONSE
        return res.status(200).send()
    }
    catch(error){
        console.log(error.message)
        // RETURN ERROR RESPONSE
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

/**
 * UPLOAD FILE TO s3 RETURN METHOD 
 * @param {*} req 
 * @param {*} res 
 */

const uploadFile = async (req,res) => { 
    try{
        return res.status(200).send(req.files[0].location)
    }
    catch(error){
        return res.status(400).send(JSON.stringify({errormessage: error.message}))
    }
}

module.exports = {
    createCategories,uploadFile,deleteMultiplePosts,deleteAllPosts,deletePost,deletePosts,updateStatus,udpatePostStatuses,
    updatePost,updateMultiplePosts,getPostDetailWithComments,getMultiplePostsDetailWithComments,
    getPostDetail,createMultiplePosts,createPost,getPosts,getCategories
}